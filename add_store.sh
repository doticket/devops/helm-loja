#!/bin/bash

add_store() {
  local ID=$1
  local HOST=$2
  local APP_NAME="api-store-$ID"
  local REPLICAS=$3

  # Adicionar a nova loja ao values.yaml
  yq eval ".lojas += [{\"id\": $ID, \"host\": \"$HOST\", \"replicas\": $REPLICAS}]" -i values.yaml


  # Commit e push das mudanças para o repositório Git
  git add .
  git commit -m "Adicionar loja $ID"
  git push

  # Criar um novo aplicativo no Argo CD para a nova loja
  argocd app create $APP_NAME \
    --repo https://gitlab.com/doticket/devops/helm-loja.git \
    --path . \
    --dest-server https://35.237.171.139 \
    --parameter id=$ID \
    --parameter host=$HOST \
    --parameter replicas=$REPLICAS \
    --dest-namespace store \
    --sync-policy automated \
    --sync-option CreateNamespace=true \
    --sync-option ApplyOutOfSyncOnly=true

  argocd app sync $APP_NAME
}

# Exemplo de uso: add_store <id> <host>
add_store $1 $2 $3